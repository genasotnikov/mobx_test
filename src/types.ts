export type DisplayedRow = {
    firstName: string,
    middleName: string,
    lastName: string,
    [key: string]: string
}

export type Row = {
    _id: string,
    data: DisplayedRow
}
