import styled from "styled-components";

const cellStyle = `
    border-style: solid;
    border-width: 0 1px 1px 0;
    border-color: white;
`;

export const Thead = styled.thead`
`;

export const Th = styled.th`
    background: #BCEBDD;
    color: white;
    text-shadow: 0 1px 1px #2D2020;
    padding: 10px 20px;
    ${cellStyle}
`;

export const Table = styled.table`
    font-family: "Lucida Sans Unicode", "Lucida Grande", Sans-Serif;
    font-size: 14px;
    border-radius: 10px;
    border-spacing: 0;
    text-align: center;
`;

export const Tbody = styled.tbody``;

export const Tr = styled.tr`
`;

export const Td = styled.td`
    padding: 10px 20px;
    background: #F8E391;
    ${cellStyle}
`;


