import React from "react";
import { Thead, Th } from "./styled";

const TableHeader = () => {
    return (
        <Thead>
            <Th>firstName</Th>
            <Th>middleName</Th>
            <Th>lastName</Th>
            <Th />
            <Th />
        </Thead>
    )
};

export default TableHeader;
