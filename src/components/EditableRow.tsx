import React from "react";
import { observer } from "mobx-react";
import { Tr, Td } from "./styled";

import {Row} from "../types";
import EditableRowState from "../stores/EditableRowState";

interface EditableRowProps {
    row: Row
}

@observer
class EditableRow extends React.Component<EditableRowProps, any> {

    store = new EditableRowState();

    componentDidMount() {
        this.store.data = this.props.row;
    }

    render() {
        if (!(this.store.data._id && this.store.data.data)) return null;
        const { data, setField, disabled } = this.store;
        return (
            <Tr>
                <Td>
                    <input disabled={disabled} value={data.data.firstName} onChange={e => setField("firstName", e.target.value)} />
                </Td>
                <Td>
                    <input disabled={disabled} value={data.data.middleName} onChange={e => setField("middleName", e.target.value)} />
                </Td>
                <Td>
                    <input disabled={disabled} value={data.data.lastName} onChange={e => setField("lastName", e.target.value)} />
                </Td>
                <Td>
                    <button onClick={this.store.updateRow}>{disabled ? "Update" : "Save"}</button>
                </Td>
                <Td>
                    <button onClick={this.store.deleteRow}>{"delete"}</button>
                </Td>
            </Tr>
        );
    }
}

export default EditableRow;
