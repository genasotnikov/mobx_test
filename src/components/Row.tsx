import React from "react";
import { Tr, Td } from "./styled";
import { Row as IRow } from "../types";
import {observer} from "mobx-react";
import RowStore from "../stores/RowStore";

type RowProps = {
    data: IRow;
}

@observer
class Row extends React.Component<RowProps> {
    store = new RowStore();

    componentDidMount(): void {
        this.store.values = this.props.data;
    }

    render() {
        if (!this.store.values.data) return null;
        const { age, Age, Email } = this.store.values.data;
        return (
            <Tr>
                <Td>
                    <input
                        disabled={this.store.disabled}
                        onChange={e => this.store.setFieldValue("age", e.target.value)}
                        value={age} />
                </Td>
                <Td>
                    <input
                        disabled={this.store.disabled}
                        onChange={e => this.store.setFieldValue("Age", e.target.value)}
                        value={Age} />
                </Td>
                <Td>
                    <input
                        disabled={this.store.disabled}
                        onChange={e => this.store.setFieldValue("Email", e.target.value)}
                        value={Email} />
                </Td>
                <Td>
                    <button onClick={this.store.onSave}>
                        {this.store.disabled ? "update" : "save"}
                    </button>
                </Td>
            </Tr>
        );
    }
}

export default Row;
