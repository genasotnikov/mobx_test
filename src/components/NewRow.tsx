import React from "react";
import {Tr, Td} from "./styled";
import {observer, useLocalObservable} from "mobx-react";
import api from "../api";
import {DisplayedRow} from "../types";
import appStore from "../stores/appStore";

const initialRow: DisplayedRow = { firstName: "", middleName: "", lastName: "" };

const NewRow = observer(() => {

    const controller = useLocalObservable(() => ({
        disabled: true,
        newRowData: {...initialRow},
        createRow: async function () {
            if (!this.disabled) {
                try {
                    await api.createRow(this.newRowData);
                    await appStore.getData();
                    this.newRowData = {...initialRow};
                } catch (e) {}
            }
            this.disabled = !this.disabled
        },
        setField: function (key: string, value: string) {
            this.newRowData[key] = value;
        }
    }));

    return (
        <Tr>
            <Td>
                <input
                    disabled={controller.disabled}
                    value={controller.newRowData.firstName}
                    onChange={e => controller.setField("firstName", e.target.value)} />
            </Td>
            <Td>
                <input
                    disabled={controller.disabled}
                    value={controller.newRowData.middleName}
                    onChange={e => controller.setField("middleName", e.target.value)} />
            </Td>
            <Td>
                <input
                    disabled={controller.disabled}
                    value={controller.newRowData.lastName}
                    onChange={e => controller.setField("lastName", e.target.value)} />
            </Td>
            <Td>
                <button onClick={controller.createRow}>{controller.disabled ? "Create" : "Save"}</button>
            </Td>
            <Td />
        </Tr>
    )
})

export default NewRow;
