import React, {useEffect} from "react";
import { observer } from "mobx-react";
import appStore from "../stores/appStore";
import { Table as StyledTable, Tr, Td } from "./styled";
import EditableRow from "./EditableRow";
import NewRow from "./NewRow";
import TableHeader from "./TableHeader";

const Table = observer(() => {
    useEffect(() => {
        appStore.getData()
    }, [])
    return (
        <StyledTable>
            <TableHeader />
            {appStore.data.map((row) => <EditableRow row={row} />)}
            <NewRow />
        </StyledTable>
    )
})

export default Table;
