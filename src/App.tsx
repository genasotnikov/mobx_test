import React from 'react';
import Table from "./components/Table";
import {create} from "mobx-persist";
import appStore from "./stores/appStore";

const hydrate = create({
  jsonify: true,
});

function App() {

  const [isRehydrated, setIsRehydrated] = React.useState(false);
  React.useEffect(() => {
    hydrate("CRUD store", appStore)
        .then(() => setIsRehydrated(true))
        .catch((e) => {throw e});
  }, []);

  if (!isRehydrated) return null;

  return <Table />
}

export default App;
