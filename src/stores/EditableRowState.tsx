import {makeAutoObservable} from "mobx";
import api from "../api";
import {Row} from "../types";
import appStore from "./appStore";

class EditableRowState {
    constructor() {
        makeAutoObservable(this);
    }

    data: Row = {} as Row;

    disabled: boolean = true;

    setField = (key: string, value: string) => {
        this.data.data[key] = value;
    }

    updateRow = () => {
        if (!this.disabled) api.updateRow(this.data._id, this.data.data);
        this.disabled = !this.disabled
    }

    deleteRow = async () => {
        try {
            await api.deleteRow(this.data._id);
            await appStore.getData();
        } catch (e) {}
    }
}

export default EditableRowState;
