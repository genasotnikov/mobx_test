import {makeAutoObservable} from "mobx";
import api from "../api";
import {Row} from "../types";


class RowStore {
    constructor() {
        makeAutoObservable(this);
    }

    setFieldValue = (key: string, value: string) => {
        this.values.data[key] = value;
    };

    onSave = async () => {
        if (!this.disabled) await api.updateRow(this.values._id, this.values.data);
        this.disabled = !this.disabled;
    };

    values: Row = {} as Row;

    disabled: boolean = true;
}

export default RowStore;
