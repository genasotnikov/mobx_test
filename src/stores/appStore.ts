import { makeAutoObservable } from "mobx";
import { persist } from "mobx-persist";
import api from "../api";
import {Row} from "../types";

class AppStore {
    constructor() {
        makeAutoObservable(this);
    }

    @persist("list")
    data: Row[] = [];

    getData = () => {
        api.getData().then((res) =>
            this.data = res.data.filter(row => (row?.data?.firstName || row?.data?.middleName || row?.data?.lastName)));
    }
}

const appStore = new AppStore();

export default appStore;
