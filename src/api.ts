import axios, {AxiosResponse} from "axios";
import {DisplayedRow, Row} from "./types";

const client = axios.create({ baseURL: "http://178.128.196.163:3000/api/records" });

const getData: () => Promise<AxiosResponse<Row[]>> = () => client.get("");

const createRow = (newData: DisplayedRow) => client.put("", {data: newData});

const updateRow = (id: string, newData: DisplayedRow) => client.post("/" + id, {data: newData});

const deleteRow = (id: string) => client.delete("/" + id);

export default {
    getData,
    updateRow,
    deleteRow,
    createRow
}
